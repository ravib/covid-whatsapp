const venom = require('venom-bot');

//TODO store in localstorage
let chatState = {};

//TODO fetch from data source
let menuItems = {
    "Report": {
        "Rtpcr": [],
        "Rapid Antigen": [],
        "Blood Report": [],
        "Ct Scan": []
    },
    "Ambulance": {
        "Ambulance Any": [],
        "Ambulance With Oxygen": [],
        "Ambulance With Icu": []
    },
    "Oxygen": {
        "Oxygen Cylinder": [],
        "Oxygen Refilling": [],
        "Oxygen Concentrator For Rent": [],
        "Oxygen Concentrator Buy": []
    },
    "Medical Equipment": {
        "Oximeter": [],
        "Flow Meter/Humidifier": [],
        "Oxygen Cylinder Mask Kit": [],
        "Nebulizer": [],
        "BiPap Machine For Rent": [],
        "Ventilator Kit": [],
        "BiPap Kit": [],
        "Ventilator For Rent": [],
        "BiPap Machine Buy": [],
        "Ventilator Buy": []
    },
    "Plasma": [],
    "Medicines": {
        "Medicines": [],
        "Injections": {
            "Remdesivir": [],
            "Tocilizumab": [],
            "Amphoterecin": [],
            "Other": []
        },
        "Fabiflu": []
    },
    "Home Visit": {
        "Any Doctor": [],
        "MD": []
    },
    "Teleadvice": [],
    "Hospitals": {
        "Isolation Centres": [],
        "Hospital Bed": {
            "With Oxygen Bed": [],
            "Icu With Ventilator": [],
            "Icu With BiPap": []
        },
        "Non Covid Hospital": []
    },
    "Food": {
        "Food(Free)": [],
        "Food(Paid)": []
    }
}

function init() {

}

init();

venom
    .create()
    .then((client) => start(client))
    .catch((erro) => {
        console.log(erro);
    });

/*
 * message states
 * 0 - waiting for help
 * 1 - "help" recieved, main menu
 * 2 - menu selection done, fetching data from backend
 * 3 - data sent to client, asked for feedback
 * 4 - feedback recieved, to send to backend
 */
function start(client) {
    client.onMessage((message) => {
        //console.log('!!!sender: ', message.sender); //sender info
        //console.log('!!!from: ', message.from); //from info
        var sendMessage = false;
        var d = menuItems;
        if (message.body.toLowerCase() === 'help' && message.isGroupMsg === false) {
            console.log("help activated");
            if (!(message.from in chatState))
                chatState[message.from] = {};
            chatState[message.from]["state"] = 1;
            chatState[message.from]["selection"] = [];
            sendMessage = true;
        } else if (!isNaN(message.body)) {
            if (chatState[message.from]['state'] === 1) {
                var n = +message.body;

                var depth = 0;

                while (depth < chatState[message.from]['selection'].length) {
                    //chatState[message.from]['selection'][depth] gives the user selection at that menu level, subtract 1 for 0 indexing
                    d = d[Object.keys(d)[chatState[message.from]['selection'][depth] - 1]];
                    depth++;
                }
                //console.log("number selection");
                //console.log((d));
                //console.log(Object.keys(d));

                //if n greater than 0 and less than length of options, store
                if (n > 0 && n <= Object.keys(d).length) {
                    chatState[message.from]['selection'].push(n);
                    d2 = d[Object.keys(d)[chatState[message.from]['selection'][depth] - 1]];
                    if (Object.keys(d2).length === 0) {
                        chatState[message.from]["state"] = 2;
                        sendMessage = true;
                    } else {
                        d = d2;
                    }
                    sendMessage = true;
                } else {
                    client
                        .sendText(message.from, "Invalid number")
                        .then((result) => {
                            console.log('Result: ', result); //return object success
                        })
                        .catch((erro) => {
                            console.error('Error when sending: ', erro); //return object error
                        });
                }
            }
        }
        //console.log("flag: "+sendMessage.toString());
        if (sendMessage) {
            console.log("sending message");
            var str = '';
            if (chatState[message.from]['state'] === 1) {
                console.log("sending menu");
                var i = 1;
                for (const [key, value] of Object.entries(d)) {
                    str += ((i).toString() + " - " + key);
                    if (Object.keys(value).length > 0) {
                        str += "*";
                    }
                    str += '\n';
                    i++;
                }
                str += "Please enter you selection:"
            } else if (chatState[message.from]['state'] === 2) {
                console.log("sending data");
                console.log(d);
                // TODO fetch contact from backend and change state
                // send post request wto backend with all data
                // TODO add feedback logic using another state
                str += "No data for " + Object.keys(d)[chatState[message.from]['selection'][depth] - 1];
            }
            client
                .sendText(message.from, str)
                .then((result) => {
                    console.log('Result: ', result); //return object success
                })
                .catch((erro) => {
                    console.error('Error when sending: ', erro); //return object error
                });
        }
    });
}
